from .utils import get_db_handle, get_collection_handle
from bson.objectid import ObjectId
from datetime import datetime

db_handle, _ = get_db_handle('controlproductos', 'localhost', 27017, '', '')
collection_handle = get_collection_handle(db_handle, 'inventario')

def save(inventario):
    inventario["estado"] = "Entregar"
    collection_handle.insert_one(inventario)

def obtenerInventarios():
    try:
        inventarios_data = [{'id': str(inventario.get('_id')), 'nombreUsuario': inventario.get('nombreUsuario'), 'producto': inventario.get('producto'), 'numeroSerie': inventario.get('numeroSerie'), 'fecha': inventario.get('fecha'), 'estado': inventario.get('estado')} for inventario in collection_handle.find({})]
        return True, inventarios_data 
    except Exception as e:
        return False, str(e)
    
def actualizarEstadoInventario(id):
    query = {'_id': ObjectId(id)}
    elementoEncontrado = collection_handle.find_one(query)
    if elementoEncontrado['estado'] == 'Entregado':
        try:
            fechaModificacion = str(elementoEncontrado['fechaModificacion']).split(':')[0] + ':' + str(elementoEncontrado['fechaModificacion']).split(':')[1]
        except:
            fechaModificacion = ''
        return False, [], fechaModificacion
    newVal = {'$set': {"estado": "Entregado", "fechaModificacion": str(datetime.now())}}
    try:
        collection_handle.update_one(query, newVal)
        inventarios_data = [{'id': str(inventario.get('_id')), 'nombreUsuario': inventario.get('nombreUsuario'), 'producto': inventario.get('producto'), 'numeroSerie': inventario.get('numeroSerie'), 'fecha': inventario.get('fecha'), 'estado': inventario.get('estado'), 'fechaModificacion': inventario.get('fechaModificacion')} for inventario in collection_handle.find({})]
        return True, inventarios_data
    except Exception as e:
        return False, str(e)