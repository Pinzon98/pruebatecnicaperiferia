from django.urls import path
from . import views
from .constants import url_constants as urlConst

urlpatterns = [
    path(urlConst.BASE_DATOS, views.baseDatos, name=urlConst.BASE_DATOS),
    path(urlConst.REGISTRO_INVENTARIO, views.registroInventario, name=urlConst.REGISTRO_INVENTARIO),
    path(urlConst.ENTREGA_INVENTARIO, views.entregaInventario, name=urlConst.ENTREGA_INVENTARIO),
    path(urlConst.OBTENER_PRODUCTOS, views.obtenerProductos, name=urlConst.OBTENER_PRODUCTOS),
    path(urlConst.OBTENER_INVENTARIOS, views.obtenerTodosInventarios, name=urlConst.OBTENER_INVENTARIOS),
    path(urlConst.REGISTRO_PRODUCTO, views.registroProducto, name=urlConst.REGISTRO_PRODUCTO)
]