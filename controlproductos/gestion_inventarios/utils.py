from datetime import datetime
from pymongo import MongoClient
from .constants import validation_constants as vc
from .models import Producto

format = '%Y-%m-%d'

def generar_datos():
    if Producto.objects.count() == 0:
        lista = [
            Producto(nombre = "Tarjeta Crédito"),
            Producto(nombre = "Cheque"),
            Producto(nombre = "Talonario")
        ]
        for item in lista:
            item.save()

def get_db_handle(db_name, host, port, username, password):
    client = MongoClient(host=host,
                         port=int(port),
                         username=username,
                         password=password
                        )
    db_handle = client[db_name]
    return db_handle, client

def get_collection_handle(db_handle,collection_name):
    return db_handle[collection_name]

def verificarRegistroInventario(body):
    listaCampos = []
    usuario = body['usuario']
    producto = body['producto']
    fecha = body['fecha']
    numeroSerie = body['numeroSerie']
    if not verificarCampoTexto(usuario):
        listaCampos.append('Usuario')
    if not verificarCampoTexto(producto):
        listaCampos.append('Producto')
    if not verificarCampoTexto(fecha):
        listaCampos.append('Fecha')
    if not verificarCampoTexto(numeroSerie):
        listaCampos.append('Numero de Serie')
    if len(listaCampos) > 0:
        mensaje = vc.FALTAN_CAMPOS
        for idx in range(len(listaCampos)):
            if idx == len(listaCampos)-1:
                mensaje = mensaje + listaCampos[idx] + "."
            else:    
                mensaje = mensaje + listaCampos[idx] + ", "
        return False, mensaje 
    try:
        bool(datetime.strptime(fecha, format))
    except:
        return False, vc.FECHA_INCORRECTA
    if numeroSerie < 0:
        return False, vc.NUMERO_SERIE_INVALIDO
    if datetime.strptime(fecha, format).date() > datetime.now().date():
        return False, vc.FECHA_REGISTRO_INVALIDO
    return True, ""

def verificarRegistroProducto(body):
    listaCampos = []
    producto = body['producto']
    if not verificarCampoTexto(producto):
        listaCampos.append('Producto')
    if len(listaCampos) > 0:
        mensaje = vc.FALTAN_CAMPOS
        for campo in listaCampos:
            mensaje = mensaje + campo + "."
        return False, mensaje
    return True, ""

def verificarCampoTexto(campo):
    if campo == None or str(campo).strip() == '':
        return False
    else:
        return True