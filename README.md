# pruebaTecnicaPeriferia

## Para empezar

- El Back esta construido en python 11
- El Front esta construido en angular 16
- Descargue el repositorio
- Base de Datos Relacional:
  1. Descargue Mysql
  2. Establezca una nueva conexión
  3. En nombre de la conexión ponga el nombre que guste
  4. El hostname debe ser 127.0.0.1 y el puerto 3306
  5. El usuario debe ser root y la clave 1234
  6. Ingrese a la base de datos que acaba de crear y el la parte de esquemas haga click derecho y haga click sobre crear esquema
  7. El esquema debe tener como nombre controlproductos y haga click sobre aplicar para generarlo
- Base de Datos No Relacional:
  1. Descargue MongoDb Compass
  2. Genere una nueva conexión con la url mongodb://localhost:27017
  3. Cree una nueva base de datos que tenga como nombre controlproductos
  4. Cree una nueva colección con el nombre de inventario
- Proyecto Python (Back):
  1. Ejecute el comando pip install -r requirements.txt para descargar todas las librerias que usa el proyecto
  2. Ejecute el comando python manage.py makemigrations
  3. Ejecute el comando python manage.py migrate
  4. Para ejecutar el back en la terminal debe ejecutar el comando python .\manage.py runserver
- Proyecto Angular (Front):
  1. Ejecute el comando npm i
  2. Ejecute el comando ng s
  3. En el navegador de su preferencia abra el link http://localhost:4200/

# Composición del programa

1. La aplicación tiene 3 tabs en la parte superior: Registro de Inventarios, Entrega de Inventarios y Crear Producto.
2. En el momento que ingresa en la aplicación si no hay productos en la base de datos relacional crea 3 productos (Tarjeta de Crédito, Cheque y Talonario)
3. En el tab de Crear Producto es posible crear un nuevo producto ingresando el nombre del nuevo producto. Si ya existe ese nombre en la base de datos relacional muestra un mensaje de error donde indica que ya se encuentra el producto creado.
4. En el tab Registro de Inventarios debe llenar todos los datos y al oprimir el boton Guardar y enviar el almacena el registro en la base de datos no relacional
5. En el tab de entrega de inventario podra ver todos los inventarión registrados.
6. Se puede observar que cada inventario registrado tiene un boton que dice si el inventario fue entregado o si se puede entregar. Si uno hace click sobre el boton de entregar cambiara inmediatamente el estado a entregar y si hace click sobre el boton del inventario que ya fue entregado aparece un mensaje donde se menciona que el inventario ya fue entregado.
